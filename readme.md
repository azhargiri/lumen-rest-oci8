## RESTful Service Server

A web service using RESTful and Oracle Database

## Installation

Install the packages
```composer install```

Create ```.env``` file
```cp .env.example .env```

Edit .env file with appropriate value. For example, you can use 
setting below to connect to oracle database

```
DB_CONNECTION=oracle
DB_HOST=localhost            # change with your oracle host/IP
DB_PORT=1521                 # change with your oracle port
DB_DATABASE=yourdatabase     # change with your oracle database/schema
DB_USERNAME=oracleusername   # change with your oracle username
DB_PASSWORD=oraclepassword   # change with your oracle password
```

## Lumen

This project is powered by Lumen, a microframework from Laravel

See [lumen.readme](lumen.readme.md)
